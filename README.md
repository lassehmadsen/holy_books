# NLP excercise on The Bible and The Quran

This repository is for an article I was working on about differences in the languages used in The Bible and The Quran.

It is possible, that others might want to use the data and code snippets for their own projects, which would be nice. Here's an overview of what you can find here, that might be useful:

*  [Vignette](https://rpubs.com/lassehjorthmadsen/605604) containing my notes with details and various explorations (in Danish). 
*  Dataset: `data/quran_cleaned.rmd`  The Quran, in English, ready for analysis. Prepared by ZuhaibAli at [Kaggle]( https://www.kaggle.com/zohaib1111/quran-dataset).
*  Dataset: `data/bible_cleaned.rmd` The King James Version of The Bible, cleaned and ready for analysis. Obtained from [Project Gutenberg](https://www.gutenberg.org/ebooks/10900url).
*  Dataset: `data/koranen_cleaned.rmd`  The Quran, in Danish, cleaned and ready for analysis. Translation by Amér Majid, 2015.
*  Dataset: `data/bibelen_cleaned.rmd`  The Bible, in Danish, cleaned and ready for analysis. The official translation from 1992, 2011 edition.
*  Dataset: `data/all_hp_books.rmd`  The Harry Potter books, in English, used to test ideas and compare with the Bible/Quran. Obtained from one of the Harry Potter packages on [Github](https://github.com/bradleyboehmke/harrypotter).   
*  Meta data: `data/bible_books.xlsx`  A list of books in the Bible, mapping the English and the Danish chapter titles.
*  Meta data: `data/quran_books.xlsx`  A list of chapters (sura) in the Quran, mapping the English and the Danish  titles.
*  R-code: `code/01_get_data.R`  R-code for downloading the King James Verion and the Harry Potter books. Contains also som details on the other texts used.
*  R-code: `code/02_clean_data.R`  R-code for cleaning all texts used. This is sometimes a little involved, as the ePub-files can be a bit messy.
*  R-code: `code/03_bible_quran_notes.Rmd`  Rmarkdown-code for the  [Vignette](https://rpubs.com/lassehjorthmadsen/605604). So you can see the full code.
*  R-code: `code/04_charts.R`  R-code for making a nice [dumbbell plot](https://gitlab.com/lassehmadsen/holy_books/-/blob/master/output/bible_violence.png).