# Functions

top_loadings <- function(tfidf, pca_model) {
  
  # Terms with highest loadings on two primary dimensions
  all_stems <- tfidf %>% 
    distinct(stem, .keep_all = TRUE) %>% 
    select(stem, common_origin, n_total)
  
  top_loadings <- data.frame(pca_model$rotation[, 1:2]) %>% 
    rownames_to_column(var = "stem") %>% 
    as_tibble() %>% 
    left_join(all_stems) %>% 
    arrange(-n_total) %>% 
    mutate(rank1 = rank(PC1, ties.method = "first"),
           rank2 = rank(PC2, ties.method = "first")) %>% 
    filter(rank1 < 11 | rank2 < 11) %>%
    arrange(-abs(PC1)) %>% 
    mutate(label = ifelse(stem != common_origin, 
                          paste0(stem, " (", common_origin, ")"), 
                          stem)) 
}

loadings_plot <- function (top_loadings_tbl) {
  
  stem_order <- top_loadings_tbl %>% select(label, PC1) %>% arrange(PC1) %>% pull(label)
  
  p <- top_loadings_tbl %>% 
    pivot_longer(c(PC1, PC2), names_to = "PC", values_to = "value") %>% 
    ggplot(aes(x = label, y = value, fill = factor(PC))) +
    geom_bar(stat = "identity", position = "stack") +
    coord_flip() +
    scale_x_discrete(limits = stem_order) +
    labs(x = NULL, y = "Factor loading") + 
    theme(axis.text.y = element_text(size = rel(0.8)),
          legend.title = element_blank(),
          legend.position = "top")
  
  return(p)
}

pca_plot <- function (dtm, pca_model, thres = 3, color, label) {
  # Takes a document-term-matrix and a corresponding
  # PCA analysis (two principal components) and returns
  # a 2d plot, with labeled outliers
  color <- enquo(color)
  label <- enquo(label)
  
  p <- dtm %>% 
    select(!! color, !! label) %>% 
    cbind(predict(object = pca_model)[, 1:2]) %>% 
    as_tibble() %>% 
    mutate(outlier = abs((PC1 - mean(PC1))) > thres * sd(PC1) |
             abs((PC2 - mean(PC2))) > thres * sd(PC2)) %>%  
    mutate(label = ifelse(outlier, !! label, NA)) %>%   
    ggplot(aes(x = PC1, y = PC2, color = !! color, label = label)) +
    geom_point(alpha = 0.5) +
    geom_text_repel(show.legend = FALSE, force = 10) +
    theme(legend.title = element_blank(),
          legend.position = "top")
  
  return(p)
}


scree_plot <- function(pca_model) {
  p <- summary(pca_model)$importance %>% 
    t() %>% 
    as.data.frame() %>%
    mutate(dimensions = row_number()) %>%
    ggplot(aes(x = dimensions, y = `Proportion of Variance` * 100)) +
    geom_point() +
    geom_line() +
    labs(y = "Proportion of total variance (percent)",
         x = "Number of dimensions")
  
  return(p)
}


doc_term_table <- function(tbl, input, lang, doc_id, ...) {
  # Tokenizes text into a document-stem matrix, and
  # and various statistics, like counts of words and 
  # documents at various levels of aggregation.
  # Also tf, idf, and tf*idf.
  
  input    <- enquo(input)
  doc_id   <- enquo(doc_id)
  doc_vars <- enquos(...)    # Any additional information about docs
  
  # This df contains additional information about each doc besides doc_id
  # We later merge this back on the doc_term table
  doc_info <- tbl %>% 
    select(!!! doc_vars, !! doc_id) %>% 
    distinct(!! doc_id, .keep_all = TRUE)
  
  tbl %>% 
    unnest_tokens(output = word, !! input) %>%                            # tokenization     
    anti_join(tibble(word = tm::stopwords(kind = lang)), by = "word") %>% # stopword removal
    mutate(stem = SnowballC::wordStem(word, language = lang)) %>%         # stemming
    group_by(stem, word) %>%                                              # for identifying words that generated that stem
    mutate(n = n()) %>% 
    group_by(stem) %>% 
    mutate(n_total = n(),
           common_origin = first(word, order_by = -n),                    # common_origin: Most frequent origin word
           no_origins = n_distinct(word)) %>%                             # no_origin: number of origins behind stem
    count(!! doc_id, stem, name = "n_doc",                                # n_doc: The counts of each stem within each doc
          n_total, common_origin, no_origins) %>%
    group_by(stem) %>%
    mutate(doc_count = n()) %>%                                           # doc_count: The number of docs with this stem
    group_by(!! doc_id) %>% 
    mutate(doc_total = sum(n_doc)) %>%                                    # doc_total: The number of words in each book
    ungroup() %>% 
    bind_tf_idf(term = stem, document = !! doc_id, n = n_doc) %>%         # get the if, idf, and tf*idf
    left_join(doc_info)                                                   # restore extra doc info we want to keep
}

doc_term_matrix <- function(tbl, term, value, ...) {
  term     <- enquo(term)
  value    <- enquo(value)
  doc_vars <- enquos(...)
  
  tbl %>% filter(!! value != 0) %>% 
    select(!!! doc_vars, !! term, !! value) %>% 
    spread(!! term, !! value, fill = 0)
}

pca_dtm <- function (tbl) {
  # Perform PCA with all numerical metrics
  tbl %>% 
    select_if(is.numeric) %>% 
    prcomp(center = TRUE, scale = TRUE)
}
