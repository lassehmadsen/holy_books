# Experiments with word vectors

library(tidyverse)
library(quanteda)
library(text2vec)
library(quanteda.corpora)

# The holy books
bib <- readRDS("data/bibelen_cleaned.rds") %>% 
  select(text, era = testament, book, chapter, verse)

kor <- readRDS("data/koranen_cleaned.rds") %>% 
  select(text, era = time, book = part, chapter = surano, verse) %>% 
  mutate(book = as.character(book))

holy <- bind_rows(bib, kor) %>% 
  mutate(collection = ifelse(str_detect(era, "Testamente"), "Bibelen", "Koranen"))

remove(bib, kor)

# Names of features above a threshold
holy_corpus <- corpus(paste(holy$text, collapse = " "))

toks <- tokens(holy_corpus, 
               what = "word", 
               remove_numbers = TRUE,
               remove_punct = TRUE,
               remove_symbols = TRUE) %>% 
  tokens_tolower()

toks_dfm <- dfm(toks, verbose = TRUE) %>%
  dfm_trim(min_termfreq = 5) %>% 
  dfm_tolower()

feats <- toks_dfm %>% featnames()
toks_dfm %>% featfreq() %>% sort(decreasing = TRUE) %>% head(50)

# leave the pads so that non-adjacent words will not become adjacent
toks <- tokens_select(toks, feats, padding = TRUE)
holy_fcm <- fcm(toks, context = "window", count = "weighted", window = 5, weights = 1 / (1:5), tri = TRUE)

# Fit the GloVe model using text2vec.
glove <- GlobalVectors$new(word_vectors_size = 125, vocabulary = featnames(holy_fcm), x_max = 10)
holy_main <- fit_transform(holy_fcm, glove, n_iter = 30)

# The two vectors are main and context. 
# According to the Glove paper, averaging the two word vectors 
# results in more accurate representation.
holy_context <- glove$components
dim(holy_context)
dim(holy_main)
holy_vectors <- as.dfm(holy_main + t(holy_context))
dim(holy_vectors)
remove(holy_context, holy_main)

# Find synonyms:
synonyms <- textstat_simil(holy_vectors, margin = "documents", 
                           method = "cosine", min_simil = 0.5)

best_synonyms <- synonyms %>% 
  data.frame() %>% 
  as_tibble() %>% 
  arrange(-cosine) %>% 
  head(20)

best_synonyms

# calculate the similarity
test <- holy_vectors["herren", ]
cos_sim <- textstat_simil(holy_vectors, test, margin = "documents", method = "cosine")
head(sort(cos_sim[, 1], decreasing = TRUE), 5)

# Tokenize by sentences
holy_sents <- holy %>% 
  group_by(collection = if_else(str_detect(era, "Testament"), "Bibelen", "Koranen")) %>% 
  summarise(text = paste(text, collapse = " ")) %>% 
  corpus(text_field = "text", docid_field = "collection") %>% 
  tokens(what = "sentence") %>% 
  unlist() %>% 
  tibble(text = ., collection = names(.)) %>% 
  mutate(id = row_number(),
         collection = str_extract(collection, "[:alpha:]+"))

# Convert sentences to dfm
holy_sents_dfm <-  holy_sents %>%   
  corpus(text_field = "text", docid_field = "id") %>% 
  dfm(remove_numbers = TRUE,
      remove_punct = TRUE,
      remove_symbols = TRUE) %>% 
  dfm_trim(min_termfreq = 5, verbose = TRUE)

# holy_sents_dfm <- holy_sents_dfm %>% 
#   dfm_subset(rowSums(holy_sents_dfm != 0) >= 11)

dim(holy_sents_dfm)
dim(holy_vectors)

dn <- docnames(holy_vectors) 
fn <- featnames(holy_sents_dfm) 
identical(dn, fn)

# Relaxed Word Movers Distance model  
rwmd_model = RWMD$new(as.matrix(holy_vectors))

bible_sample <- sample(1:36352, 2000)
quran_sample <- sample(36353:46859, 2000)

rwmd_dist = dist2(holy_sents_dfm[bible_sample, ],
                  holy_sents_dfm[quran_sample, ], 
                  method = rwmd_model, norm = 'none')

rownames(rwmd_dist) <- bible_sample
colnames(rwmd_dist) <- quran_sample

# Find similar sentences
similar_sent_rwmd <- rwmd_dist %>% 
  reshape2::melt(value.name = "rwmd") %>% 
  as_tibble() %>% 
  filter(Var1 != Var2) %>% 
  arrange(rwmd) %>% 
  left_join(holy_sents, by = c("Var1" = "id")) %>% 
  left_join(holy_sents, by = c("Var2" = "id"), suffix = c("1", "2")) %>% 
  filter(collection1 != collection2)

similar_sent_rwmd %>% 
  head(5) %>% 
  select(-starts_with("Var")) %>% 
  knitr::kable(digits = 2) %>% 
  kableExtra::kable_styling(bootstrap_options = "striped", full_width = F) %>% 
  kableExtra::column_spec(2, width = "30em") %>% 
  kableExtra::column_spec(4, width = "30em") 

